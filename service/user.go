package service

import (
	// "authService/models"
	manager "auth_service/postgres/manager"
	"database/sql"

	"github.com/google/uuid"
	pb "auth_service/genproto/auth_service"
)

type UserService struct {
	UM manager.UserManager
}

func NewUserService(conn *sql.DB) *UserService {
	return &UserService{UM: *manager.NewUserManager(conn)}
}

func (u *UserService) Register(req *pb.UserCreate) error {
	req.UserId = uuid.NewString()
	return u.UM.Register(req)
}

func (u *UserService) GetProfile(id *pb.ById) (*pb.UserCreate, error) {
	return u.UM.Profile(id)
}

func (u *UserService) LoginUser(req *pb.LoginReq) (*pb.Token, error) {
	return u.UM.LoginUser(req)
}
