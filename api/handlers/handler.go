package handlers

import (
	"auth_service/service"
)

type HTTPHandler struct {
	US *service.UserService
}

func NewHandler(us *service.UserService) *HTTPHandler {
	return &HTTPHandler{US: us}
}
