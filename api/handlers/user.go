package handlers

import (
	token "auth_service/token"
	// "auth_service/models"
	// "fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	// "github.com/golang-jwt/jwt"
	_ "github.com/swaggo/swag"
	// "golang.org/x/crypto/bcrypt"
	pb "auth_service/genproto/auth_service"
)

// @Router 				/register [post]
// @Summary 			REGISTER USER
// @Description		 	This api registers user
// @Tags 				USER
// @Accept 				json
// @Produce 			json
// @Param data 			body pb.UserModel true "User"
// @Success 201 		{object} string "successfully registered"
// @Failure 400 		string Error
func (h *HTTPHandler)RegisterUser(c *gin.Context) {
	var body pb.UserModel
    if err := c.ShouldBindJSON(&body); err!= nil {
        c.JSON(400, gin.H{"error": err.Error()})
        return
    }
	req := pb.UserCreate{
        Username:   body.Username,
        Email:      body.Email,
        Password:    body.Password,
	}
    err := h.US.Register(&req)
    if err!= nil {
        c.JSON(400, gin.H{"error": err.Error()})
        return
    }
    c.JSON(200, gin.H{"message":"successfully registered"})
}
// @Router 				/login [post]
// @Summary 			Login USER
// @Description		 	This api logs  user in
// @Tags 				USER
// @Accept 				json
// @Produce 			json
// @Param data 			body pb.LoginReq true "LoginReq"
// @Success 201 		{object} pb.Token
// @Failure 400 		string Error
func (h *HTTPHandler)LoginUser(c *gin.Context) {
	var req pb.LoginReq
    if err := c.ShouldBindJSON(&req); err!= nil {
        c.JSON(400, gin.H{"error": err.Error()})
        return
    }
    token, err := h.US.LoginUser(&req)
    if err!= nil {
        c.JSON(400, gin.H{"error": err.Error()})
        return
    }
    c.JSON(200, token)
}

// @Router 				/user/info [GET]
// @Summary 			Gets User info
// @Description		 	This api Gets User info
// @Tags 				User
// @Accept 				json
// @Produce 			json
// @Security            BearerAuth
// @Success 200 		{object} pb.UserCreate
// @Failure 400 		string Error
func (h *HTTPHandler) GetUserInfo(c *gin.Context) {
	tok := c.GetHeader("Authorization")
	if tok == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization token is required"})
		return
	}
	claims, err := token.ExtractClaim(tok)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
		return
	}
	user_id, ok := claims["user_id"].(string)
	if !ok || user_id == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Username not found in token"})
		return
	}
	user := &pb.ById{Id: user_id}
	res, err := h.US.GetProfile(user)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, res)
}