package postgresql

import (
	"auth_service/config"
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

func ConnectDB(cf *config.Config) (*sql.DB, error) {
	dbConn := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", cf.PostgresUser, cf.PostgresPassword, cf.PostgresHost, cf.PostgresPort, cf.PostgresDatabase)
	db, err := sql.Open("postgres", dbConn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}
