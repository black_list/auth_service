package managers

import (
	// "auth-service/models"
	pb "auth_service/genproto/auth_service"
	"database/sql"
	"errors"
	"auth_service/token"
)

type UserManager struct {
	Conn *sql.DB
}

func NewUserManager(db *sql.DB) *UserManager {
	return &UserManager{Conn: db}
}

func (m *UserManager) Register(req *pb.UserCreate) error {
	query := "INSERT INTO users (user_id, username, role, email, password) VALUES ($1, $2, $3, $4,  $5) RETURNING user_id"
	var id string
	err := m.Conn.QueryRow(query, req.UserId, req.Username, "user", req.Email, req.Password).Scan(&id)
	if err != nil {
		return err
	}
	return nil
}

func (m *UserManager) Profile(req *pb.ById) (*pb.UserCreate, error) {
	query := "SELECT user_id, username, email, password FROM users WHERE user_id = $1"
	user := &pb.UserCreate{}
	err := m.Conn.QueryRow(query, req.Id).Scan(&user.UserId, &user.Username, &user.Email, &user.Password)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *UserManager) LoginUser(logreq *pb.LoginReq) (*pb.Token, error) {
	var usernameDB, passwordDB, user_id, role string
	query := `select user_id, username, password,role from users where username = $1`
	err := s.Conn.QueryRow(query, logreq.Username).Scan(&user_id, &usernameDB, &passwordDB, &role)
	if err != nil {
		return nil, errors.New("incorrect login credentials")
	}
	qualify := true
	if passwordDB != logreq.Password || usernameDB != logreq.Username {
		qualify = false
	}
	if !qualify {
		return nil, errors.New("username or password incorrect")
	}
	token := token.GenerateJWTToken(user_id,usernameDB,role)
	if err != nil {
		return nil, err
	}
	return token, nil
}
