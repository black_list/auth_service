package managers

import (
	"testing"
	"time"
	"database/sql"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"auth_service/models"
)

// Mock function for NewUserRepo - replace this with your actual implementation
func NewUserRepo(db *sql.DB) *UserManager {
	return &UserManager{Conn: db}
}

func TestUserRepo_Register(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	userRepo := NewUserRepo(db)

	user := models.RegisterReq{
		Username: "testuser",
		Password: "password",
		Email:    "testuser@example.com",
	}

	// Define the expected query and result
	mock.ExpectExec("INSERT INTO users \\(username, email, password\\) VALUES \\(\\$1, \\$2, \\$3\\) RETURNING id").
		WithArgs(user.Username, user.Email, user.Password).
		WillReturnResult(sqlmock.NewResult(1, 1))

	// Call the Register method
	res, err := userRepo.Register(user)

	// Assert the result
	assert.NoError(t, err)
	assert.Equal(t, int64(1), res.ID)
	assert.Equal(t, "testuser", res.Username)
}

func TestUserRepo_Login(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	userRepo := NewUserRepo(db)

	user := models.LoginReq{
		Email:    "testuser@example.com",
		Password: "password",
	}

	// Define the expected query and result
	mock.ExpectQuery("SELECT id, username FROM users WHERE email = \\$1 AND password = \\$2").
		WithArgs(user.Email, user.Password).
		WillReturnRows(sqlmock.NewRows([]string{"id", "username"}).AddRow(1, "testuser"))

	// Call the Login method
	res, err := userRepo.Login(user)

	// Assert the result
	assert.NoError(t, err)
	assert.Equal(t, int64(1), res.ID)
	assert.Equal(t, "testuser", res.Username)
}

func TestUserRepo_GetByUsername(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	userRepo := NewUserRepo(db)

	username := "testuser"

	// Define the expected query and result
	mock.ExpectQuery("SELECT \\* FROM users WHERE username = \\$1").
		WithArgs(username).
		WillReturnRows(sqlmock.NewRows([]string{"id", "username", "email", "password", "created_at", "updated_at", "deleted_at"}).
			AddRow(1, username, "testuser@example.com", "password", time.Now(), time.Now(), nil))

	// Call the GetByUsername method
	res, err := userRepo.GetByUsername(username)

	// Assert the result
	assert.NoError(t, err)
	assert.Equal(t, int64(1), res.ID)
	assert.Equal(t, "testuser", res.Username)
	assert.Equal(t, "testuser@example.com", res.Email)
}
