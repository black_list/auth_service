package main

import (
	"auth_service/api"
	"auth_service/api/handlers"
	"auth_service/config"
	"auth_service/postgres"
	"auth_service/service"
)

func main() {
	cf := config.Load()

	conn, err := postgresql.ConnectDB(&cf)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	us := service.NewUserService(conn)
	handler := handlers.NewHandler(us)

	roter := api.NewGin(handler)
	if err := roter.Run(cf.AUTH_PORT); err != nil {
		panic(err)
	}
}
